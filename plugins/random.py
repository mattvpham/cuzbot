import random
import re

TOKENS = [
  ('CHOICES', r'.*\|.*'),
  ('MULTIROLL', r'[0-9]+d[0-9]+'),
  ('ROLL', r'd[0-9]+'),
  ('RANGE', r'\(?-?[0-9]+\)?-\(?-?[0-9]+\)?'),
  ('INTEGER', r'[0-9]+'),
]

class RandomPlugin():
  cmd = '!random'
  help_message = f'''`{cmd}` - Generate pseudo-random numbers.
    EXAMPLE: `{cmd} a|b|c|d` - Choose one option from `a`, `b`, `c`, or `d`
    EXAMPLE: `{cmd} 5` - Generate an integer between 1 and 5 (inclusive)
    EXAMPLE: `{cmd} 3-100` - Generate an integer between 3 and 100 (inclusive)
    EXAMPLE: `{cmd} d20` - Generate an integer between 1 and 20 (roll a 20-sided die)
    EXAMPLE: `{cmd} 3d6` - Generate three integers between 1 and 6, then sum them together (roll three 6-sided dice)
  '''

  def init(self, client):
    self.client = client
    token_regex = '|'.join(f'(?P<{token_name}>{token_re})' for (token_name, token_re) in TOKENS)
    self.re = token_regex
    print('Initialized RandomPlugin')

  def process(self, word):
    # Some people, when confronted with a problem, think "I know, I'll use regular expressions." Now they have two problems.
    match_obj = re.match(self.re, word)

    kind = None
    result = None
    if match_obj:
      kind = match_obj.lastgroup
      value = match_obj.group()

      if kind == 'CHOICES':
        choices = value.split('|')
        choices = [choice.strip() for choice in choices]
        choices = [choice for choice in choices if choice != '']
        result = random.choice(choices)
      elif kind == 'INTEGER':
        value = int(value)
        result = random.randint(1, value)
      elif kind == 'RANGE':
        # Remove whitespace and parens
        value = re.sub('[()\s]', '', value)

        values = re.split(r'(-?[0-9]+)-(-?[0-9]+)', value)
        values = [val for val in values if val != '']

        start, end = values
        start = int(start)
        end = int(end)
        result = random.randint(start, end)
      elif kind == 'ROLL':
        _, d_max = value.split('d')
        d_max = int(d_max)
        result = random.randint(1, d_max)
      elif kind == 'MULTIROLL':
        d_num, d_max = value.split('d')
        d_num = int(d_num)
        d_max = int(d_max)
        result = 0
        for i in range(d_num):
          roll = random.randint(1, d_max)
          result += roll

    return (kind, result)

  async def on_message(self, message):
    if message.author == self.client.user:
      return

    if message.content.startswith(self.cmd):
      content = message.content
      args = content[len(self.cmd)+1:]
      args = args.strip()

      try:
        kind, result = self.process(args)
      except (ValueError, IndexError):
        await message.channel.send(self.help_message)
        return

      if kind is None or result is None:
        await message.channel.send(self.help_message)
        return

      response_text = f'{result}'

      await message.channel.send(response_text)
