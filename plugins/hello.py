class HelloPlugin():
  help_message = '`!hello` - Sends a greeting'

  def init(self, client):
    self.client = client
    print('Initialized HelloPlugin')

  async def on_message(self, message):
    if message.author == self.client.user:
        return

    if message.content.startswith('!hello'):
        await message.channel.send('Hello!')