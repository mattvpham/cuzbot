class ErrorPlugin():
  cmd = '!error'
  help_message = f'''`{cmd}` - Generates an exception.'''

  def init(self, client):
    self.client = client

  async def on_message(self, message):
    if message.author == self.client.user:
        return

    if message.content.startswith(self.cmd):
        raise UserWarning