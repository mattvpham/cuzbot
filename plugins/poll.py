import string

letters = [
  '🇦',
  '🇧',
  '🇨',
  '🇩',
  '🇪',
  '🇫',
  '🇬',
  '🇭',
  'ℹ️',
  '🇯',
  '🇰',
  '🇱',
  '🇲',
  '🇳',
  '🇴',
  '🇵',
  '🇶',
  '🇷',
  '🇸',
  '🇹',
  '🇺',
  '🇼',
  '🇽',
  '🇾',
  '🇿',
]

def get_emoji(pos):
  try:
    letter = letters[pos]
    emoji = letter
  except IndexError:
    emoji = '❓'
  return emoji

class PollPlugin():
  cmd = '!poll'
  help_message = f'''`{cmd}` - Starts a poll.
    USAGE: send `{cmd}` followed by a pipe (|) separated list.
    EXAMPLE: `{cmd} What is your favorite fruit?|apple|orange|banana` will create a poll with a choice of three fruits.
  '''

  def init(self, client):
    self.client = client
    print('Initialized PollPlugin')

  async def on_message(self, message):
    if message.author == self.client.user:
      return

    if message.content.startswith(self.cmd):
      content = message.content
      args = content[len(self.cmd)+1:]
      args = args.strip()
      poll_options = args.split('|')

      if len(poll_options) < 1:
        await message.channel.send(self.help_message)
        return

      response_text = f'**{poll_options[0]}**\n\n'
      for i, opt in enumerate(poll_options[1:]):
        response_text += f'{get_emoji(i)} : `{opt.strip()}`\n'

      response_text += '\nTo vote, react below.'
      response_message = await message.channel.send(response_text)

      for i, _ in enumerate(poll_options[1:]):
        await response_message.add_reaction(get_emoji(i))
