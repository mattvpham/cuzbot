import os
from os.path import join, dirname
import traceback
from dotenv import load_dotenv
import discord
import plugins.hello as hello
import plugins.poll as poll
import plugins.random as random
import plugins.error as error
import signal

# Environment variables are stored in a .env file in this directory
load_dotenv(join(dirname(__file__), '.env'))
BOT_TOKEN = os.getenv('BOT_TOKEN')

client = discord.Client()

plugins = [
  hello.HelloPlugin(),
  poll.PollPlugin(),
  random.RandomPlugin(),
  error.ErrorPlugin(),
]

for plugin in plugins:
  plugin.init(client)

@client.event
async def on_ready():
  print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    # The bot should ignore its own messages
    if message.author == client.user:
        return

    if message.content.startswith('!help'):
      messages = []
      for plugin in plugins:
        messages.append(plugin.help_message)
      await message.channel.send('\n'.join(messages))

    # Go through all our plugins and react accordingly
    for plugin in plugins:
      try:
        await plugin.on_message(message)
      except:
        tb = traceback.format_exc()
        response_text = f'''Uh oh, something went wrong.\n\n>>> {tb}'''
        await message.channel.send(response_text)

client.run(BOT_TOKEN)
